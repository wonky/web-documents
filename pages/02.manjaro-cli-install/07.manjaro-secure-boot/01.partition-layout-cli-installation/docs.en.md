---
title: 'Base CLI install'
taxonomy:
    category:
        - docs
published: true
---

I have never realized the the potential of LVM and never really cared.

In fact I have considered LVM to be an unnecessary complication but as @openminded recommends LVM - I am prepared to put aside my prejudices and embrace LVM for this lesson.

I should apply myself to learn to create the LVM structure by hand, I think this will have to wait as [`manjaro-architect`][1] is a great tool and I don't feel like diving into LVM just yet. Manjaro Architect can be added to the running instance of a live ISO. Ensure you have a working mirror and the package databases. Do not use the update argument or the gui as this will attempt to update the live ISO which makes no sense at all.

```

pacman -Sy
pacman -S manjaro-architect
```

The base install needed to implement secure boot

1. create 2 partitions
   - efi system (0xEF00)
   - default linux filesystem (0x8300)
3. create a luks container on the linux partition
4. open the luks container
   - inside the luks container
      - create a volume group
      - create 3 logical volumes
         - root (40G)
         - swap (4G)
         - home (remaining space)
5. Format $esp using fat32
6. Format the lvroot using ext4
7. Format the lvhome using ext4
8. Mount lvroot as /
9. Mount $esp /boot
10. Mount lvhome as /home
11. Install base CLI system using systemd boot loader (GUI is not needed right now)
12. Reboot system to verify it works
14. Create `/efi` and modify fstab $esp mount to `/efi`

Don't forget to verify networking


[1]: https://iso.uex.dk/nix-architect/