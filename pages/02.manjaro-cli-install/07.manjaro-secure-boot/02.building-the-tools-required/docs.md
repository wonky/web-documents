---
title: 'Building the tools required'
taxonomy:
    category:
        - docs
---

The following pages contains mostly of quotes from the original topic - refactored to suit my reading style.

@openminded lists some required tools. Some of the tools is only available as PKGBUILDS which can be fetched from AUR. 

Tools from the repo
* sbsigntools
* efitools
* tpm2-tools

The tools I need to buiild are (although tpm2-top and plymouth is in the official repo I will go with the doc)

* tpm2-totp-git
* plymouth-git
* sbupdate-git
* shim-signed

There is a couple of methods available - either using an AUR helper or do it the Arch way

Manjaro's package manager Pamac
```

pamac install sbsigntools efitools tpm2-tools
pamac build tpm2-totp-git plymouth-git sbupdate-git shim-signed
```

The swiss army knife `yay`

    yay -Syu sbsigntools efitools tpm2-tools tpm2-totp-git sbupdate-git shim-signed
    
Arch way - using `git` and `makepkg`. In this case you must ensure the package group `base-devel` is available as well

    pacman -Syu sbsigntools efitools tpm2-tools git base-devel --needed
    
Then a small script
```

#!/usr/bin/env bash
tools=('tpm2-totp-git' 'plymouth-git' 'sbupdate-git' 'shim-signed')
for tool in ${tools[@]}; do
    git clone https://aur.archlinux.org/${tool}.git
    cd tpm2-totp-git
    makepkg -is
    cd ..
done
```