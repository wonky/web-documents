---
title: 'No passwordless root on Arch'
taxonomy:
    category:
        - docs
published: true
date: '30-10-2020 15:24'
publish_date: '30-10-2020 15:24'
---

Just need a place to remember the link

For the sake of security Archlinux (based) distributions disables passwordless root login[[1]].

From [Archlinux git][1]

> ### Set a default root password.
> This will prevent root login with an empty password on a fresh Arch Linux
> installation.
> 
> This is only about the default behaviour, you could restore the previous one by
> running `passwd -d root`'.
> Please note, this is not recommended and behave inconsistenly between
> applications.
> 
> We use a trick in the shadow file to set a default password which never allow
> login by using this password.
> 
> The special value `*` is used in the shadow file.
> We don't use `!`, `!!`, `!*` on purpose.
> The special `!` char, which should mean password locked (and not account locked)
> is interpreted by some applications (e.g. sshd) as an account locked and will
> prevent root login.
> 
> This change was suggested by Lennart Poettering and Zbigniew Jedrzejewski-Szmek
> to security@archlinux.org.

```
/etc/shadow
```
```
root:*:14871::::::
```

[1]: https://git.archlinux.org/svntogit/packages.git/commit/trunk?h=packages/filesystem&id=0320c909f3867d47576083e853543bab1705185b