---
title: 'CRM lingo'
language: da
taxonomy:
    category:
        - docs
---

### Kontakt
Det enkleste af alt er kontakter mennesker. Ligesom i din adressebog, kan en CRM’s kontaktpersoner indeholde navne og personlige oplysninger på dine kunder og kunder. Du vil sandsynligvis også se firma eller konto i din CRM sammen med kontakter; disse er specialiserede kontakter for de organisationer, du arbejder med, og du vil sandsynligvis linke dine individuelle kontakter til en virksomhed eller en konto.
### Interesse
Nogle kontakter er specielle: Det er mennesker, der ser ud til at ville lave forretninger med din virksomhed i fremtiden. Interesse er de mennesker, som du vil være særlig opmærksom på.
### Mulighed
Det viser sig, at den kundeemne var virkelig interesseret, og du tror, du vil være i stand til at sælge dem dit produkt eller din tjeneste. Nu er de en mulighed, en person der sandsynligvis køber dit produkt, og du vil gerne angive oplysninger om, hvad denne mulighed er, og spore den i din CRM.
### Tilbud
Du har arbejdet med en kontaktperson, og forvandlet et kundeemne til en mulighed, og nu er du næsten klar til at indgå en aftale - nu giver du dem en pris på den service eller de produkter, de får for det. Det er, hvad Tilbud er til - stedet at angive den pris, du citerede til potentielle kunder, ikke stedet at gemme dine inspirerende forretningstilbud.
### Salg
Alt gik godt, og du har solgt dit produkt - eller måske ikke, og muligheden faldt igennem. Du sporer begge dem med tilbud, som viser dine vundne og mistede tilbud.
### Profil
Typisk vil det være de mennesker i dit eget firma, der bruger CRM-appen. Hver af dem kan have en rolle eller et bestemt sæt tilladelser i appen - dit salgsteam har muligvis ikke adgang til din leverandørliste, siger, mens måske kun HR kan redigere detaljer på dine teamprofiler.
### Kampagne
Du bruger sandsynligvis brug af en CRM til markedsføring, og det er her, du sporer dit opsøgende arbejde. Kampagner er, hvor du sporer dit marketingarbejde. Hver kampagne viser de kontakter og virksomheder, der er mest vigtige for denne marketingkampagne, sammen med resultater, noter og mere.
### Etiket
Svarende til tags i Gmail eller metadata på dine fotos giver tags dig en måde at tilføje ekstra information til en kontaktperson, en aftale eller andet i din virksomheds CRM. Disse ekstra data giver dig flere måder at filtrere og sortere gennem din CRM.
### Aktivitet
Aktivitet i en CRM refererer typisk til alt, hvad der er sket i appen - nye tilbud, kontakter, muligheder eller måske bare en besked fra dine kolleger. Aktivitet er normalt angivet i et Facebook-lignende nyhedsfeed, så du nemt kan se over dem.

