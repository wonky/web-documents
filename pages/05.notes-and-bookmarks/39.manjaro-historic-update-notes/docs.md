---
title: 'Manjaro historic update notes'
date: '12-08-2018 15:38'
taxonomy:
    category:
        - docs
---

# Update an old Manjaro installation

The notes is for Openbox 0.8.13.1 64-bit ISO subsequent update to latest packages and LTS kernel.

The notes is assumed to work with other older Manjaro installations as well.

## Revision history
* 2019-09-01: Reworked to avoid pacman seg-fault.
* 2019-08-30: Added info on Xfce 15.12 ISO.
* 2019-08-29: Revised to match repo status.
* 2018-08-12: Initial document.

[Download as pdf](./update-old-manjaro-installation.pdf)

## Tips

### Console (tty)
* Open a console <kbd>Ctrl</kbd><kbd>F4</kbd>
Note: You can use any F-key between F2 and F6
* To repeat a previous command in console use <kbd>&uarr;</kbd> and/or <kbd>&darr;</kbd> to locate the command
* To edit the command use <kbd>&rarr;</kbd>, <kbd>&larr;</kbd>, <kbd>Home</kbd> and <kbd>End</kbd> to navigate the line.

### Update process
During an update you will get different messages. If you do not accept a removal or replacement by pressing **y** the process stops.

* Accept replacing packages (default is `Yes`) 
* Accept removing packages (default is `No`)

### Nano editor
* To delete a line <kbd>Ctrl</kbd><kbd>k</kbd>
* To save and exit <kbd>F2</kbd><kbd>y</kbd><kbd>Enter</kbd>)

## Console upgrade
Boot your system - **Don't login** - switch to console (tty) and login as **root**.

## Preparation
### Mirrorlist
Find an updated mirror [repo.manjaro.org](https://repo.manjaro.org) which is close to you on, and edit the first entry in `/etc/pacman.d/mirrorlist` - leave `/stable/$repo/$arch` as it is.

Example

    https://mirrors.manjaro.org/repo/stable/$repo/$arch

Delete all other lines and save the file 

    nano /etc/pacman.d/mirrorlist

### Pacman
Modify *pacman.conf* and comment the line *SyncFirst =* and save the change.

    nano /etc/pacman.conf

## Update
### Package database
Synchronizing databases

    pacman -Syy

### Keyring
Install the new keyrings for Arch and Manjaro and populate pacman key database

    pacman -S archlinux-keyring manjaro-keyring
    pacman-key --populate archlinux manjaro

### Core packages
Update core packages

    pacman -S pacman-static ca-certificates openssl openssl-1.0 pacman-mirrors

### Remove old package
Remove old package versions. If you get dependency errors, repeat the command and add the mentioned packages.

    pacman-static -R package-query pamac yaourt

For a 15.12 ISO the following command must be executed

    pacman-static -R package-query pamac yaourt pacli update-notifier bmenu gstreamer0.10 gstreamer0.10-base gstreamer0.10-bad gstreamer0.10-good gstreamer0.10-ugly gstreamer0.10-base-plugins gstreamer0.10-bad-plugins gstreamer0.10-good-plugins gstreamer0.10-ugly-plugins wxgtk xfce4-mixer guayadeque manjaro-welcome thunar-archive-plugin qpdfview-ps-plugin qpdfview-djvu-plugin qpdfview plymouth

Edit ***/etc/mkinitcpio.conf** and remove *plymouth* from from the **HOOKS** line then rebuild - use uname -a to finde the correct kernel.

    mkinitcpio -p linux41

### Conflicting files
Before you begin some files will create conflicts. Commands to remove known conflicts are noted below. You may have different conflicts which needs to be solved before you can update.

    rm -f /usr/{lib,lib32}/{libEGL.so*,libGL.so*,libGLESv*}
    rm -f /usr/lib/xorg/modules/extensions/libglx.so
    rm -f /etc/fonts/conf.d/{20-unhint-*,57-dejavu-*}
    rm -fr /var/lib/pacman/local/xorg-mkfontscale*
    rm -fr /var/lib/pacman/local/ttf-dejavu*
    rm -f /usr/bin/mkfontscale
    rm -fr /usr/share/licenses/xorg-mkfontscale
    rm -f /usr/share/man/mans/mkfontscale*
    mv /usr/lib32/libcurl.so.4 /usr/lib32/libcurl.so.4.bak
    
### Update
Update the system installing a new LTS kernel - if you are running in virtualbox select the correct guest-modules package based on the kernel selected.

At this point we cannot remove the ***ca-certificates.crt*** file because this will break pacman's ability to download from a https enabled mirror. As the version of pacman is an older version it supports the ***--force*** argument which we need to replace the file.

    pacman-static -Syu linux419 --force

Read the messages - if the default action is **Y** you can press enter. Otherwise select **y** to confirm package removal.

## Congratulation
You have succesfully upgraded a very old installation.

If you are lucky you won't have graphical issues.

Suggestions:
* remove plymouth
   * remove from mkinitcpio.conf
   * change display-manager command
   * change display-manager (openbox uses the now unmaintained slim)