---
title: 'Use Python to download and verify Manjaro ISO'
date: '08:45 23-08-2023'
taxonomy:
    category:
        - docs
---

## Download and verify a Manjaro ISO.

When I wrote the [topic on verifying the ISO signature] I vaguely recalled a shell script in the repo **manjaro-iso-downloader** but it doesn't work - at least not for me.

So I decided to wrap up some python code.

The tool provides a convenient way of downloading and verifying an ISO as they are listed on [manjaro.org/download][4].

Neither Sway nor ARM is supported, sway because the files is located elsewhere and ARM because there is no signature.

The script has options to fetch the latest stable release review ISO or unstable developer previews from Manjaro Github.

The source is FOSS, and you are encouraged to clone my repo at [scm.nix.dk][2].

Besides the basic Python modules - the script relies on the Python requests 3 module and p7zip. You can check if you have p7zip installed using

which 7z

Manjaro

On Manjaro you don’t need to install the requests module - it is present as a dependency of pacman-mirrors.

You may need to install p7zip depending on the result of above check

sudo pacman -Syu p7zip

Other Linux

If you are using another Linux you can use requirements.txt to install the necessary dependency.

You will also need to fetch the public key for Manjaro Build Server

gpg --recv-keys 3B794DE6D4320FCE594F4171279E7CF5D8D56EC8

Use your systems package manager to install p7zip.

### Setup
Create the folder **~/.local/bin**

    mkdir -p ~/.local/bin

Then create a new file in this bin folder - name the file **get-iso** - then use your favorite text editor to copy paste [the code][1] into the new file.

Make the file executable

    chmod +x ~/.local/bin/get-iso

## Usage
```
  $ get-iso -h
usage: get-iso [-h] [-f] [-r | -p] {plasma,xfce,gnome,budgie,cinnamon,i3,mate}

This tool will download a named Manjaro ISO

positional arguments:
  {plasma,xfce,gnome,budgie,cinnamon,i3,mate}
                        Edition e.g. plasma or xfce.

options:
  -h, --help            show this help message and exit
  -f, --full            Download full ISO

Previews:
  -r, --review          Get Latest Release Review ISO
  -p, --preview         Get Latest Developer Preview ISO

get-iso v. 0.8 - GPL v3 or later <https://www.gnu.org/licenses/gpl.html>

```
get-iso v. 0.6 - GPL v3 or later <https://www.gnu.org/licenses/gpl.html>
The script defaults to pull the minimal ISO and downloaded files is placed your home folder. 

Example downloading **full mate** edition
```
get-iso mate -f
```
```
Downloading: manjaro-mate-22.0-230104-linux61.iso
Downloading: manjaro-mate-22.0-230104-linux61.iso.sig
Wait for verification ...
gpg: assuming signed data in 'manjaro-mate-22.0-230104-linux61.iso'
gpg: Signature made ons 04 jan 2023 12:48:04 CET
gpg:                using RSA key 3B794DE6D4320FCE594F4171279E7CF5D8D56EC8
gpg: Good signature from "Manjaro Build Server <build@manjaro.org>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 3B79 4DE6 D432 0FCE 594F  4171 279E 7CF5 D8D5 6EC8
```

Another example downloading **minimal plasma**
```
get-iso plasma
```
```
 $ get-iso plasma
Downloading: manjaro-kde-22.1.3-minimal-230529-linux61.iso
Downloading: manjaro-kde-22.1.3-minimal-230529-linux61.iso.sig
Wait for verification ...
gpg: assuming signed data in 'manjaro-kde-22.1.3-minimal-230529-linux61.iso'
gpg: Signature made man 29 maj 2023 11:46:55 CEST
gpg:                using RSA key 3B794DE6D4320FCE594F4171279E7CF5D8D56EC8
gpg: Good signature from "Manjaro Build Server <build@manjaro.org>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 3B79 4DE6 D432 0FCE 594F  4171 279E 7CF5 D8D5 6EC8
```


## The complete script

[http://scm.nix.dk/root/manjaro-get-iso/src/branch/main/get-iso](http://scm.nix.dk/root/manjaro-get-iso/src/branch/main/get-iso)

## Crosspost
Posted at [forum.manjaro.org/t/root-tip-utility-script-using-python-to-download-and-verify-a-manjaro-iso][1]

[1]: https://forum.manjaro.org/t/root-tip-utility-script-using-python-to-download-and-verify-a-manjaro-iso/146703
[2]: http://scm.nix.dk/root/manjaro-get-iso
[3]: https://forum.manjaro.org/t/root-tip-how-to-forum-mini-guide-to-verify-iso-signature/146680
[4]: https://manjaro.org/download