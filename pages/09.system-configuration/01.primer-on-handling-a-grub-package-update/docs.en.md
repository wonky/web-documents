---
title: 'Primer on handling a grub package update'
date: '10:57 25-12-2023'
taxonomy:
    category:
        - docs
---

## How To update the system boot after updating grub itself
### Manjaro Linux
Posted at [Manjaro Forum][forum]

With the grub update to v2.12 and it was thus advised to ensure all parts of grub on the system is populated with the new binary and updated configuration.

This has revealed that a lot users have doubts on how to correctly identify if their system needs special handling e.g. identifying whether to use EFI or BIOS commands.

This topic is aimed at being a primer to properly assess how to update grub on a system given the type of boot loader installation.

## System type
To deduct whether you system is booted in EFI or BIOS mode

List the content of the efi firmware folder
```
ls /sys/firmware/efi/efivars
```
If you get an error or an empty list - your system is booted in BIOS mode and you should skip to the [bios-section]

Verification - should return a similar output
```
 $ lsblk -o path,pttype,parttypename | grep -e 'EFI'
/dev/nvme1n1p1 gpt    EFI System
```

## EFI/GPT system
If your system is EFI it is simple as you do not have to bother pointing the installation to a specific device.

Reinstall grub on a default Manjaro Linux EFI system
```
sudo grub-install --target=x86_64-efi --efi-directory=/boot/efi --recheck --bootloader-id=Manjaro
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

## BIOS system
The most common BIOS type is using BIOS/MBR.

**As a rule of thumb**: the device containing your system root will likely be the primary boot device. This may not be true for all systems and if not then you know how to handle this anyway.

```
lsblk -o path,pttype,parttypename | grep -e 'dos' -e 'BIOS'
```

The output will show you devices and partitions which may be used to hold your grub bootloader.

**NOTE:** Keep in mind this is examples - your system will be different and you will have to adapt the following to your result.

### BIOS/MBR system

With an output like this example grub is installed to the device Master Boot Record on the device **never** to the partition
```
/dev/sdy       dos
/dev/sdy1      dos    Linux
```

Reinstall grub on a Manjaro Linux BIOS/MBR system pointing to device MBR:
*(Don't forget to replace **/dev/sdy** with the correct block device name!)*
```
grub-install --target=i386-pc --recheck /dev/sdy
sudo grub-mkconfig -o /boot/grub/grub.cfg
```


### BIOS/GPT system

If you are getting a response like this you are using a BIOS/GPT system and grub is installed to an empty partition
```
/dev/sdy1      gpt    BIOS boot
```

Reinstall grub on a Manjaro Linux BIOS/GPT system pointing to device BIOS boot partition
```
grub-install --target=i386-pc --recheck /dev/sdy1
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

[bios-section]: #bios-system
[forum]: https://forum.manjaro.org/t/root-tip-how-to-primer-on-handling-a-grub-packge-update/154003